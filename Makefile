CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= role_unit/role_unit_containers
TARGETS = $(wildcard archlinux ubuntu[0-9]* debian[0-9]* centos[0-9]* build)
CENTOS_TARGETS = $(patsubst %,%_cached,$(wildcard centos[0-9]*))
REPOSITORY_NAME = $$(echo ${CI_REGISTRY}/${CI_PROJECT_PATH} | tr '[:upper:]' '[:lower:]')
DOCKER_CMD = $$(command -v buildah >/dev/null 2>&1 && echo buildah ||echo docker)

containers: $(TARGETS)

$(TARGETS): login
	echo $@
	BUILDAH_FORMAT=docker $(DOCKER_CMD) build -t ${REPOSITORY_NAME}:$@ -f $@/dockerfile $@
	command -v buildah >/dev/null 2>&1 && BUILDAH_FORMAT=docker buildah commit --squash $(buildah from "${REPOSITORY_NAME}:$@") "${REPOSITORY_NAME}:$@" || true
	test "${CI_COMMIT_REF_NAME}" == "master" && $(DOCKER_CMD) push ${REGISTRY_PATH} ${REPOSITORY_NAME}:$@ || true

$(CENTOS_TARGETS): login
	echo $@
	BUILDAH_FORMAT=docker $(DOCKER_CMD) build --build-arg CENTOS_VERSION=$$(echo $@ | sed 's/_cached$$//') -t ${REPOSITORY_NAME}:$@ -f centos_cached/dockerfile centos_cached
	command -v buildah >/dev/null 2>&1 && BUILDAH_FORMAT=docker buildah commit --squash $(buildah from "${REPOSITORY_NAME}:$@") "${REPOSITORY_NAME}:$@" || true
	test "${CI_COMMIT_REF_NAME}" == "master" && $(DOCKER_CMD) push ${REGISTRY_PATH} ${REPOSITORY_NAME}:$@ || true

login:
	test -n "${CI_BUILD_TOKEN}" && $(DOCKER_CMD) login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} || true

.PHONY: $(TARGETS)
