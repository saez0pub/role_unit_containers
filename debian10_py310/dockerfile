FROM registry.gitlab.com/role_unit/role_unit_containers:debian10

MAINTAINER n0vember <n0vember@half-9.net>

# Environment
ARG VERSION=3.10.4
ENV DIR=Python-${VERSION}
ENV ARCHIVE=${DIR}.tar.xz
ENV URL=https://www.python.org/ftp/python/${VERSION}/${ARCHIVE}

ENV BUILD_PACKAGES="build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev libbz2-dev"

# Creating build environment
RUN mkdir -p /build/${DIR} /build-tmp
ENV TMPDIR /build-tmp
WORKDIR /build

# Proceeding python compilation
RUN echo "===== build pre-requisites =====" && \
    apt-get update && \
    apt-get install -y ${BUILD_PACKAGES} && \
    echo "===== retrieving source code =====" && \
    curl -o ${ARCHIVE} ${URL} && \
    tar -xaf ${ARCHIVE} && \
    echo "===== compiling =====" && \
    bash -c "cd ${DIR} ; ./configure --enable-optimizations" && \
    make -C ${DIR} -j 4 && \
    make -C ${DIR} altinstall && \
    echo "===== cleaning up =====" && \
    apt-get purge -y ${BUILD_PACKAGES} && \
    apt-get autoremove -y && \
    apt-get autoclean -y && \
    apt-get clean -y && \
    rm -rf ${DIR} ${ARCHIVE} && \
    echo "===== done ====="

# Set back base image behaviour
RUN rm -rf /build /build-tmp
WORKDIR /workdir
ENV TMPDIR /tmp

# Information
RUN python3.10 --version
